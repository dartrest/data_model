import 'package:data_model/src/extensions.dart';
import 'package:data_model/src/json_encodable.dart';
import 'package:test/test.dart';

class TestJsonEncodable implements JsonEncodable {
  String string;

  List list;

  Map map;

  TestJsonEncodable({this.string, this.list, this.map});

  @override
  Map<String, dynamic> get json =>
      {'string': string, 'list': list, 'map': map}..removeEmpty();
}

void main() {
  test('remove empty', () {
    expect(TestJsonEncodable(string: null, list: [], map: {}).json, {});
  });

  test('list of json encodables to list of jsons', () {
    final jsonEncodables = [
      TestJsonEncodable(
          string: 'string', list: [1, 2, 3], map: {'key': 'value'}),
      TestJsonEncodable(string: null, list: [], map: {})
    ];
    expect(jsonEncodables.toJson(),
        jsonEncodables.map((encodable) => encodable.json).toList());
  });
}
