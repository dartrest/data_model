## 1.0.1

- ObjectId now not abstract

## 1.0.0

- Implemented `JsonEncodable` interface and `ObjectId` base class
