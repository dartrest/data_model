import 'json_encodable.dart';

extension JsonListExtension on List<JsonEncodable> {
  /// Returns list of jsons
  List toJson() => map((object) => object.json)?.toList();
}

extension MapExtension on Map {
  /// Removes all null values, empty lists and empty maps
  void removeEmpty() => removeWhere((key, value) {
        return value == null ||
            (value is Iterable && value.isEmpty) ||
            (value is Map && value.isEmpty);
      });
}
