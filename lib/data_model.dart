/// Base tools for creating data models.
library data_model;

export 'src/extensions.dart';
export 'src/json_encodable.dart';
export 'src/model.dart';
export 'src/object_id.dart';
